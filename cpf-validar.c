#include <stdio.h>
// #include <math.h>
#include <time.h>
#include <stdlib.h>

// void numero_para_arranjo(int num, unsigned char arr[], int tam);
void string_para_arranjo(char str[], int arr[]);
void encontrar_digitos_verificadores(int arr[9], int *out_v1, int *out_v2);

int main(int argc, char *argv[])
{
    int cpf[11];

    if (argc < 2)
    {
        printf("Por favor, passe o CPF como argumento do programa.\r\n");
        printf("EX: ./cpf-validar 12345678909\r\n    CPF valido\r\n");

        return 0;
    }

    // Hacks para circunverter o maior inteiro possivel em C
//    char cpf_0_ate_5[7];
//    for (int i = 0; i < 6; i++)
//        cpf_0_ate_5[i] = argv[1][i];
//    cpf_0_ate_5[6] = '\0';

//    char cpf_6_ate_10[6];
//    for (int i = 0; i < 5; i++)
//        cpf_6_ate_10[i] = argv[1][i + 6];
//    cpf_6_ate_10[5] = '\0';

//    numero_para_arranjo(atoi(cpf_0_ate_5), cpf + 0, 6);
//    numero_para_arranjo(atoi(cpf_6_ate_10), cpf + 6 * sizeof(unsigned char), 5);

    string_para_arranjo(argv[1], cpf);

    int v1;
    int v2;

    encontrar_digitos_verificadores(cpf, &v1, &v2);

    if (cpf[9] == v1 && cpf[10] == v2)
        printf("CPF valido\n");
    else
        printf("CPF invalido\n");

    return 0;
}

// void numero_para_arranjo(int num, unsigned char arr[], int tam)
// {
//     for (int i = 0; i < tam; i++)
//     {
//         arr[i] = (num / ((int) (pow(10, tam - i - 1) + 0.1))) % 10;
//     }
// }

void string_para_arranjo(char str[], int arr[])
{
    for (int i = 0; str[i] != '\0'; i++)
    {
        arr[i] = str[i] - '0';
    }
}

void encontrar_digitos_verificadores(int arr[9], int *out_v1, int *out_v2)
{
    int v1 = 0;
    int v2 = 0;

    for (int i = 0; i < 9; i++)
    {
        v1 += arr[i] * (10 - i);
        v2 += arr[i] * (11 - i);
    }

    v1 %= 11;
    if (v1 < 2)
        v1 = 0;
    else
        v1 = 11 - v1;

    v2 += v1 * 2;
    v2 %= 11;
    if (v2 < 2)
        v2 = 0;
    else
        v2 = 11 - v2;

    *out_v1 = v1;
    *out_v2 = v2;
}

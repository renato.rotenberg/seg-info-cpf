(defun number->list (n)
  (map 'list #'digit-char-p (prin1-to-string n)))

(defun cpf-verificador (digito cpf)
  (let* ((soma (loop for num in (reverse (number->list cpf))
                     for i from 2 to (+ digito 9)
                     for cod = (* num i)
                     sum cod))
         (v (rem soma 11)))
    (if (< v 2)
        0
        (- 11 v))))

(defun cpf-gerar (&optional (cpf (random 1000000000)))
  (let* ((v1 (cpf-verificador 1 cpf))
         (v2 (cpf-verificador 2 (+ (* cpf 10) v1))))
    (+ (* cpf 100) (* v1 10) v2)))

(defun cpf-verificar (cpf)
  (= cpf (cpf-gerar (floor cpf 100))))

(defun cpf-num->string (cpf)
  (format nil "~{~a~a~a.~a~a~a.~a~a~a-~a~a~}" (number->list cpf)))

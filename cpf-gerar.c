#include <stdio.h>
// #include <math.h>
#include <time.h>
#include <stdlib.h>

// void numero_para_arranjo(int num, unsigned char arr[], int tam);
void string_para_arranjo(char str[], int arr[]);
void encontrar_digitos_verificadores(int arr[9], int *out_v1, int *out_v2);

int main(int argc, char *argv[])
{
    int cpf[9];

    if (argc > 1)
    {
        // numero_para_arranjo(atoi(argv[1]), cpf, 9);
        string_para_arranjo(argv[1], cpf);
    }
    else
    {
        srand(time(NULL));
        int r = rand() % 1000000000;
        char str[20];
        sprintf(str, "%d", r);
        string_para_arranjo(str, cpf);
        // numero_para_arranjo(r, cpf, 9);
    }

    int v1;
    int v2;

    encontrar_digitos_verificadores(cpf, &v1, &v2);

    printf("%d%d%d.%d%d%d.%d%d%d-%d%d\n", cpf[0], cpf[1], cpf[2], cpf[3],
                                          cpf[4], cpf[5], cpf[6], cpf[7],
                                          cpf[8], v1, v2);

    return 0;
}

// void numero_para_arranjo(int num, unsigned char arr[], int tam)
// {
//     for (int i = 0; i < tam; i++)
//     {
//         arr[i] = (num / ((int) (pow(10, tam - i - 1) + 0.1))) % 10;
//     }
// }

void string_para_arranjo(char str[], int arr[])
{
    for (int i = 0; str[i] != '\0'; i++)
    {
        arr[i] = str[i] - '0';
    }
}

void encontrar_digitos_verificadores(int arr[9], int *out_v1, int *out_v2)
{
    int v1 = 0;
    int v2 = 0;

    for (int i = 0; i < 9; i++)
    {
        v1 += arr[i] * (10 - i);
        v2 += arr[i] * (11 - i);
    }

    v1 %= 11;
    if (v1 < 2)
        v1 = 0;
    else
        v1 = 11 - v1;

    v2 += v1 * 2;
    v2 %= 11;
    if (v2 < 2)
        v2 = 0;
    else
        v2 = 11 - v2;

    *out_v1 = v1;
    *out_v2 = v2;
}
